/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import views.ConverterFrame;
import views.FrmMenu;
import views.NootePadFrame;

/**
 *
 * @author Jarvin
 */
public class MainController implements ActionListener{

    FrmMenu frame;
    public MainController(FrmMenu frame) {
        this.frame = frame;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
            case "Exit":
                System.exit(0);
            break;
            case "Converter":
                InvokeConverter();
            break;
            case "Manage":
                InvokeManage();
            break;    
        }
    }
    
    public void InvokeConverter(){
        ConverterFrame f = new ConverterFrame();
        frame.ShowChild(f, false);
    }
    
    public void InvokeManage(){
        NootePadFrame f = new NootePadFrame();
        frame.ShowChild(f, true);
    }
    
    
    
}
