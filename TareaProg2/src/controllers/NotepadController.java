/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import models.Textos;
import views.NootePadFrame;

/**
 *
 * @author Jarvin
 */
public class NotepadController implements ActionListener{
    private Textos texto;
    NootePadFrame pad;
    JFileChooser d;
    public NotepadController(NootePadFrame pad) {
        this.pad = pad;
        d = new JFileChooser();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
            case "Guardar":
                d.showSaveDialog(pad);
                File file = d.getSelectedFile();
                texto = pad.GetData();
                save(file);
            break;
            case "Load":
                d.showOpenDialog(pad);
                texto = open(d.getSelectedFile());
                pad.SetData();
            break;
            case "Load2":
                d.showOpenDialog(pad);
                texto = open(d.getSelectedFile());
                pad.SetData();
            break;
        }    
    }
    
    public void save(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(texto);
            w.flush();
        }catch(IOException e){
        
        }
    }
    
    public Textos open(File file){
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Textos)ois.readObject(); 
        }catch(IOException|ClassNotFoundException e){
        
        }
        return null;
    }
}
