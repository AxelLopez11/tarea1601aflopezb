/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Jarvin
 */
public class Converter {
    private double Monto;
    private double Result;
    private double cordoba;
    private double dolares;
    private double euro;

    public Converter() {
    }

    public Converter(double Monto, double Result, double cordoba, double dolares, double euro) {
        this.Monto = Monto;
        this.Result = Result;
        this.cordoba = cordoba;
        this.dolares = dolares;
        this.euro = euro;
    }

    public double getMonto() {
        return Monto;
    }

    public void setMonto(double Monto) {
        this.Monto = Monto;
    }

    public double getResult() {
        return Result;
    }

    public void setResult(double Result) {
        this.Result = Result;
    }

    public double getCordoba() {
        return cordoba;
    }

    public void setCordoba(double cordoba) {
        this.cordoba = cordoba;
    }

    public double getDolares() {
        return dolares;
    }

    public void setDolares(double dolares) {
        this.dolares = dolares;
    }

    public double getEuro() {
        return euro;
    }

    public void setEuro(double euro) {
        this.euro = euro;
    }

    
    
}
